package com.cidp.monitorsystem.service.dispservice;

import com.cidp.monitorsystem.mapper.ConnectivelyMapper;
import com.cidp.monitorsystem.model.CompleteConnectively;
import com.cidp.monitorsystem.model.Connectively;
import com.cidp.monitorsystem.model.Link;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class ConnectivelyService {
    @Autowired
    ConnectivelyMapper connectivelyMapper;


    public List<Connectively> getConnect() {
        return connectivelyMapper.getTrimConnect();
    }

    public List<Connectively> getConnectAll() {
        return connectivelyMapper.getConnect();
    }
    public Set<Link> selectAllLinks() {
        Set<Link> links = connectivelyMapper.selectAllLinksSet();
        for (Link link : links) {
            link.setDif(connectivelyMapper.selectDifByDIpAndDIfindex(link.getDip(),link.getDifindex()));
            Link slink = connectivelyMapper.selectIntfaceFlowByIpAndIfIndex(link.getSifindex(), link.getSip());
            link.setDate(slink.getDate());
            link.setSfRate(slink.getSfRate());
            link.setSflow(slink.getSflow());
            Link dlink = connectivelyMapper.selectIntfaceFlowByIpAndIfIndex(link.getDifindex(), link.getDip());
            link.setDflow(dlink.getSflow());
            link.setDfRate(dlink.getSfRate());
        }
        return links;
    }
    public boolean hasSip2Dip(String sip, String dip) {
        Connectively con = connectivelyMapper.hsaSip2Dip(sip,dip);
        if (con!=null){
            return true;
        }
        return false;
    }
    public boolean hasDip2Sip(String dip, String sip) {
        Connectively con = connectivelyMapper.hasDip2Sip(dip,sip);
        if (con!=null){
            return true;
        }
        return false;
    }

    public void addConnect(Connectively connectively) {
        connectivelyMapper.addConnect(connectively);
    }

    public List<CompleteConnectively> selectConnectively(){
        return connectivelyMapper.selectConnectively();
    }

    public int clearConnectively() {
        return connectivelyMapper.clearConnectively();
    }

    public int insertConnectively(List<CompleteConnectively> connectivelies) {
        return connectivelyMapper.insertConnectively(connectivelies);
    }
}
